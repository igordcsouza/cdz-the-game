<?php

// Pasta onde o arquivo vai ser salvo
$_UP['pasta'] = 'uploads/';

// Tamanho m&aacute;ximo do arquivo (em Bytes)
$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb

// Array com as extens&otilde;es permitidas
$_UP['extensoes'] = array('jpg', 'png', 'gif');

// Renomeia o arquivo? (Se true, o arquivo ser&aacute; salvo como .jpg e um nome &uacute;nico)
$_UP['renomeia'] = false;

// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'N&atilde;o houve erro';
$_UP['erros'][1] = 'O arquivo no upload &eacute; maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'N&atilde;o foi feito o upload do arquivo';

// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES['arquivo']['error'] != 0) {
die("N&atilde;o foi poss&iacute;vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']]);
exit; // Para a execu&ccedil;&atilde;o do script
}

// Caso script chegue a esse ponto, n&atilde;o houve erro com o upload e o PHP pode continuar

// Faz a verifica&ccedil;&atilde;o da extens&atilde;o do arquivo
$extensao = strtolower(end(explode('.', $_FILES['arquivo']['name'])));
if (array_search($extensao, $_UP['extensoes']) === false) {
echo "Por favor, envie arquivos com as seguintes extens&otilde;es: jpg, png ou gif";
}

// Faz a verifica&ccedil;&atilde;o do tamanho do arquivo
else if ($_UP['tamanho'] < $_FILES['arquivo']['size']) {
echo "O arquivo enviado &eacute; muito grande, envie arquivos de at&eacute; 2Mb.";
}

// O arquivo passou em todas as verifica&ccedil;&otilde;es, hora de tentar mov&ecirc;-lo para a pasta
else {
// Primeiro verifica se deve trocar o nome do arquivo
if ($_UP['renomeia'] == true) {
// Cria um nome baseado no UNIX TIMESTAMP atual e com extens&atilde;o .jpg
$nome_final = time().'.jpg';
} else {
// Mant&eacute;m o nome original do arquivo
$nome_final = $_FILES['arquivo']['name'];
}

// Depois verifica se &eacute; poss&iacute;vel mover o arquivo para a pasta escolhida
if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final)) {
// Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
echo "Upload efetuado com sucesso!";
echo '<br /><a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
} else {
// N&atilde;o foi poss&iacute;vel fazer o upload, provavelmente a pasta est&aacute; incorreta
echo "N&atilde;o foi poss&iacute;vel enviar o arquivo, tente novamente";
}

}

?>